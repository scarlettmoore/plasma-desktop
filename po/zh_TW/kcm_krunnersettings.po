# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Franklin, 2014.
# Jeff Huang <s8321414@gmail.com>, 2016.
# pan93412 <pan93412@gmail.com>, 2018, 2019.
# Kisaragi Hiu <mail@kisaragi-hiu.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-07-02 00:50+0000\n"
"PO-Revision-Date: 2022-06-23 10:17+0900\n"
"Last-Translator: Kisaragi Hiu <mail@kisaragi-hiu.com>\n"
"Language-Team: Chinese <kde-i18n-doc@kde.org>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 22.04.2\n"

#: package/contents/ui/main.qml:29
#, kde-format
msgid "Position on screen:"
msgstr ""

#: package/contents/ui/main.qml:33
#, kde-format
msgid "Top"
msgstr ""

#: package/contents/ui/main.qml:40
#, kde-format
msgid "Center"
msgstr ""

#: package/contents/ui/main.qml:53
#, kde-format
msgid "Activation:"
msgstr ""

#: package/contents/ui/main.qml:56
#, kde-format
msgctxt "@option:check"
msgid "Activate when pressing any key on the desktop"
msgstr ""

#: package/contents/ui/main.qml:69
#, fuzzy, kde-format
#| msgid "Clear History"
msgid "History:"
msgstr "清除歷史"

#: package/contents/ui/main.qml:72
#, kde-format
msgctxt "@option:check"
msgid "Remember past searches"
msgstr ""

#: package/contents/ui/main.qml:83
#, kde-format
msgctxt "@option:check"
msgid "Retain last search when re-opening"
msgstr ""

#: package/contents/ui/main.qml:94
#, kde-format
msgctxt "@option:check"
msgid "Activity-aware (last search and history)"
msgstr ""

#: package/contents/ui/main.qml:109
#, kde-format
msgid "Clear History"
msgstr "清除歷史"

#: package/contents/ui/main.qml:110
#, kde-format
msgctxt "@action:button %1 activity name"
msgid "Clear History for Activity \"%1\""
msgstr ""

#: package/contents/ui/main.qml:111
#, fuzzy, kde-format
#| msgid "Clear History"
msgid "Clear History…"
msgstr "清除歷史"

#: package/contents/ui/main.qml:140
#, kde-format
msgctxt "@item:inmenu delete krunner history for all activities"
msgid "For all activities"
msgstr ""

#: package/contents/ui/main.qml:153
#, kde-format
msgctxt "@item:inmenu delete krunner history for this activity"
msgid "For activity \"%1\""
msgstr ""

#: package/contents/ui/main.qml:170
#, kde-format
msgctxt "@label"
msgid "Plugins:"
msgstr ""

#: package/contents/ui/main.qml:174
#, kde-format
msgctxt "@action:button"
msgid "Configure Enabled Search Plugins…"
msgstr "設定啟用的搜尋外掛程式…"

#~ msgid "Available Plugins"
#~ msgstr "可用的外掛程式"

#~ msgid ""
#~ "Enable or disable plugins (used in KRunner, Application Launcher, and the "
#~ "Overview effect)"
#~ msgstr "啟用或停用外掛程式（在 KRunner、應用程式啟動器以及總覽效果中使用）"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Franklin Weng, Jeff Huang"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "franklin@goodhorse.idv.tw, s8321414@gmail.com"

#, fuzzy
#~| msgctxt "kcm name for About dialog"
#~| msgid "Configure Search Bar"
#~ msgctxt "kcm name for About dialog"
#~ msgid "Configure search settings"
#~ msgstr "設定搜尋框"

#~ msgid "Vishesh Handa"
#~ msgstr "Vishesh Handa"

#, fuzzy
#~| msgid "Clear History"
#~ msgid "KRunner history:"
#~ msgstr "清除歷史"

#, fuzzy
#~| msgid "Clear History"
#~ msgid "Clear History..."
#~ msgstr "清除歷史"

#~ msgid "Enable or disable KRunner plugins:"
#~ msgstr "啟用或停用 KRunner 外掛程式："
