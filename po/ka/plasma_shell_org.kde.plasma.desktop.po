# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-10 00:47+0000\n"
"PO-Revision-Date: 2022-12-09 07:55+0100\n"
"Last-Translator: Temuri Doghonadze <temuri.doghonadze@gmail.com>\n"
"Language-Team: Georgian <kde-i18n-doc@kde.org>\n"
"Language: ka\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.2.2\n"

#: contents/activitymanager/ActivityItem.qml:212
msgid "Currently being used"
msgstr "ამჟამად გამოიყენება"

#: contents/activitymanager/ActivityItem.qml:252
msgid ""
"Move to\n"
"this activity"
msgstr "ამ აქტივობასთან გადმოტანა"

#: contents/activitymanager/ActivityItem.qml:282
msgid ""
"Show also\n"
"in this activity"
msgstr "ამ აქტივობაშიც ჩვენება"

#: contents/activitymanager/ActivityItem.qml:342
msgid "Configure"
msgstr "მორგება"

#: contents/activitymanager/ActivityItem.qml:359
msgid "Stop activity"
msgstr "აქტივობის შეჩერება"

#: contents/activitymanager/ActivityList.qml:143
msgid "Stopped activities:"
msgstr "გაჩერებული აქტივობები:"

#: contents/activitymanager/ActivityManager.qml:127
msgid "Create activity…"
msgstr "აქტივობის შექმნა…"

#: contents/activitymanager/Heading.qml:61
msgid "Activities"
msgstr "ქმედებები"

#: contents/activitymanager/StoppedActivityItem.qml:133
msgid "Configure activity"
msgstr "აქტივობის მორგება"

#: contents/activitymanager/StoppedActivityItem.qml:148
msgid "Delete"
msgstr "წაშლა"

#: contents/applet/AppletError.qml:123
msgid "Sorry! There was an error loading %1."
msgstr "%1-ის ჩატვირთვის შეცდომა."

#: contents/applet/AppletError.qml:161
msgid "Copy to Clipboard"
msgstr "გაცვლის ბუფერში კოპირება"

#: contents/applet/AppletError.qml:184
msgid "View Error Details…"
msgstr "შეცდომის დეტალები ნახვა…"

#: contents/applet/CompactApplet.qml:70
msgid "Open %1"
msgstr "%1-ის გახსნა"

#: contents/configuration/AboutPlugin.qml:19
#: contents/configuration/AppletConfiguration.qml:248
msgid "About"
msgstr "შესახებ"

#: contents/configuration/AboutPlugin.qml:47
msgid "Send an email to %1"
msgstr "%1-სთვის ელფოსტის გაგზავნა"

#: contents/configuration/AboutPlugin.qml:61
msgctxt "@info:tooltip %1 url"
msgid "Open website %1"
msgstr "ვებგვერდის გახსნა: %1"

#: contents/configuration/AboutPlugin.qml:125
msgid "Copyright"
msgstr "საავტორო უფლებები"

#: contents/configuration/AboutPlugin.qml:145 contents/explorer/Tooltip.qml:93
msgid "License:"
msgstr "ლიცენზია:"

#: contents/configuration/AboutPlugin.qml:148
msgctxt "@info:whatsthis"
msgid "View license text"
msgstr "ლიცენზიის ტექსტის ნახვა"

#: contents/configuration/AboutPlugin.qml:160
msgid "Authors"
msgstr "ავტორები"

#: contents/configuration/AboutPlugin.qml:170
msgid "Credits"
msgstr "კრედიტები"

#: contents/configuration/AboutPlugin.qml:181
msgid "Translators"
msgstr "მთარგმნელები"

#: contents/configuration/AboutPlugin.qml:195
msgid "Report a Bug…"
msgstr "შეცდომის პატაკი…"

#: contents/configuration/AppletConfiguration.qml:56
msgid "Keyboard Shortcuts"
msgstr "სხარტი კლავიშები"

#: contents/configuration/AppletConfiguration.qml:296
msgid "Apply Settings"
msgstr "პარამეტრების გადატარება"

#: contents/configuration/AppletConfiguration.qml:297
msgid ""
"The settings of the current module have changed. Do you want to apply the "
"changes or discard them?"
msgstr ""
"მიმდინარე მოდულის პარამეტრები შეიცვალა. გნებავთ გადაატაროთ ეს ცვლილებები თუ "
"მოვაშოროთ ისინი?"

#: contents/configuration/AppletConfiguration.qml:327
msgid "OK"
msgstr "დიახ"

#: contents/configuration/AppletConfiguration.qml:335
msgid "Apply"
msgstr "გამოყენება"

#: contents/configuration/AppletConfiguration.qml:341
#: contents/explorer/AppletAlternatives.qml:179
msgid "Cancel"
msgstr "გაუქმება"

#: contents/configuration/ConfigCategoryDelegate.qml:28
msgid "Open configuration page"
msgstr "კონფიგურაციის გვერდის გახსნა"

#: contents/configuration/ConfigurationContainmentActions.qml:21
msgid "Left-Button"
msgstr "მარცხენა ღილაკი"

#: contents/configuration/ConfigurationContainmentActions.qml:22
msgid "Right-Button"
msgstr "მარჯვენა ღილაკი"

#: contents/configuration/ConfigurationContainmentActions.qml:23
msgid "Middle-Button"
msgstr "შუა ღილაკი"

#: contents/configuration/ConfigurationContainmentActions.qml:24
msgid "Back-Button"
msgstr "ღილაკი უკან"

#: contents/configuration/ConfigurationContainmentActions.qml:25
msgid "Forward-Button"
msgstr "ღილაკი წინ"

#: contents/configuration/ConfigurationContainmentActions.qml:27
msgid "Vertical-Scroll"
msgstr "ვერტიკალური ცოცია"

#: contents/configuration/ConfigurationContainmentActions.qml:28
msgid "Horizontal-Scroll"
msgstr "ჰორიზონტალური ცოცია"

#: contents/configuration/ConfigurationContainmentActions.qml:30
msgid "Shift"
msgstr "Shift"

#: contents/configuration/ConfigurationContainmentActions.qml:31
msgid "Ctrl"
msgstr "Ctrl"

#: contents/configuration/ConfigurationContainmentActions.qml:32
msgid "Alt"
msgstr "Alt"

#: contents/configuration/ConfigurationContainmentActions.qml:33
msgid "Meta"
msgstr "Meta"

#: contents/configuration/ConfigurationContainmentActions.qml:95
msgctxt "Concatenation sign for shortcuts, e.g. Ctrl+Shift"
msgid "+"
msgstr "+"

#: contents/configuration/ConfigurationContainmentActions.qml:167
msgctxt "@title"
msgid "About"
msgstr "შესახებ"

#: contents/configuration/ConfigurationContainmentActions.qml:182
#: contents/configuration/MouseEventInputButton.qml:13
msgid "Add Action"
msgstr "ქმედების დამატება"

#: contents/configuration/ConfigurationContainmentAppearance.qml:78
msgid "Layout changes have been restricted by the system administrator"
msgstr "განლაგების ცვლილება შეზღუდულია სისტემური ადმინისტრატორის მიერ"

#: contents/configuration/ConfigurationContainmentAppearance.qml:93
msgid "Layout:"
msgstr "განლაგება:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:107
msgid "Wallpaper type:"
msgstr "ფონური სურათის ტიპი:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:127
msgid "Get New Plugins…"
msgstr "ახალი დამატებების მიღება…"

#: contents/configuration/ConfigurationContainmentAppearance.qml:195
msgid "Layout changes must be applied before other changes can be made"
msgstr ""
"სხვა ცვლილებების განხორციელებამდე საჭიროა განლაგების ცვლილებების გადატარება"

#: contents/configuration/ConfigurationContainmentAppearance.qml:199
msgid "Apply Now"
msgstr "ახლა გადატარება"

#: contents/configuration/ConfigurationShortcuts.qml:16
msgid "Shortcuts"
msgstr "მალსახმობები"

#: contents/configuration/ConfigurationShortcuts.qml:28
msgid "This shortcut will activate the applet as though it had been clicked."
msgstr "ეს მალსახმობი გაააქტიურებს აპლეტს, თითქოს მას დააწკაპუნეთ."

#: contents/configuration/ContainmentConfiguration.qml:29
msgid "Wallpaper"
msgstr "ფონური სურათი"

#: contents/configuration/ContainmentConfiguration.qml:34
msgid "Mouse Actions"
msgstr "თაგუნას ქმედებები"

#: contents/configuration/MouseEventInputButton.qml:20
msgid "Input Here"
msgstr "აქ შეყვანეთ"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:39
#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:192
#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:192
msgid "Remove Panel"
msgstr "პანელის მოცილება"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:47
msgid "Panel Alignment"
msgstr "პანელის სწორება"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:63
msgid "Top"
msgstr "თავში"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:63
msgid "Left"
msgstr "მარცხენა"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:73
msgid "Center"
msgstr "ცენტრი"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:83
msgid "Bottom"
msgstr "ქვედა"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:83
msgid "Right"
msgstr "Right"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:93
msgid "Visibility"
msgstr "ხილვადობა"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:107
msgid "Always Visible"
msgstr "ყოველთვის ხილული"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:114
msgid "Auto Hide"
msgstr "ავტომატურად დამალვა"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:121
msgid "Windows Can Cover"
msgstr "ფანჯრებს გადაფარვა შეუძლიათ"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:128
msgid "Windows Go Below"
msgstr "ფანჯრებს ქვეშ შეძრომა შეუძლიათ"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:137
msgid "Opacity"
msgstr "გაუმჭირვალობა"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:154
msgid "Adaptive"
msgstr "შეგუებადი"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:161
msgid "Opaque"
msgstr "გაუმჭვირვალე"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:168
msgid "Translucent"
msgstr "გამჭვირვალე"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:176
msgid "Maximize Panel"
msgstr "პანელის გადიდება"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:182
msgid "Floating Panel"
msgstr "მცურავი პანელი"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:198
msgid "Shortcut"
msgstr "მალსახმობი"

#: contents/configuration/panelconfiguration/Ruler.qml:20
msgid "Drag to change maximum height."
msgstr "მაქსიმალური სიმაღლის შესაცვლელად გადაათრიეთ."

#: contents/configuration/panelconfiguration/Ruler.qml:20
msgid "Drag to change maximum width."
msgstr "მაქსიმალური სიგანის შესაცვლელად გადაათრიეთ."

#: contents/configuration/panelconfiguration/Ruler.qml:20
#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Double click to reset."
msgstr "ორმაგი დაწკაპუნება საწყის მნიშვნელობაზე დასაბრუნებლად."

#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Drag to change minimum height."
msgstr "მინიმალური სიმაღლის შესაცვლელად გადაათრიეთ."

#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Drag to change minimum width."
msgstr "მინიმალური სიგანის შესაცვლელად გადაათრიეთ."

#: contents/configuration/panelconfiguration/Ruler.qml:69
msgid ""
"Drag to change position on this screen edge.\n"
"Double click to reset."
msgstr ""
"გადაათრიეთ ეკრანის კუთხის პოზიციის შესაცვლელად.\n"
"ორმაგი წკაპი საწყის მნიშვნელობაზე დასაბრუნებლად."

#: contents/configuration/panelconfiguration/ToolBar.qml:24
msgid "Add Widgets…"
msgstr "ვიჯეტების დამატება…"

#: contents/configuration/panelconfiguration/ToolBar.qml:25
msgid "Add Spacer"
msgstr "დამშორებლის დამატება"

#: contents/configuration/panelconfiguration/ToolBar.qml:26
msgid "More Options…"
msgstr "მეტი პარამეტრი…"

#: contents/configuration/panelconfiguration/ToolBar.qml:222
msgctxt "Minimize the length of this string as much as possible"
msgid "Drag to move"
msgstr "გადასატანად გადაათრიეთ"

#: contents/configuration/panelconfiguration/ToolBar.qml:261
msgctxt "@info:tooltip"
msgid "Use arrow keys to move the panel"
msgstr "პანელის გადასატანად ისრების ღილაკები გამოიყენეთ"

#: contents/configuration/panelconfiguration/ToolBar.qml:282
msgid "Panel width:"
msgstr "პანელის სიგანე:"

#: contents/configuration/panelconfiguration/ToolBar.qml:282
msgid "Panel height:"
msgstr "პანელის სიმაღლე:"

#: contents/configuration/panelconfiguration/ToolBar.qml:402
#: contents/configuration/ShellContainmentConfiguration.qml:43
msgid "Close"
msgstr "დახურვა"

#: contents/configuration/ShellContainmentConfiguration.qml:19
msgid "Panels and Desktops Management"
msgstr "სამუშაო მაგიდებისა და პანელების მართვა"

#: contents/configuration/ShellContainmentConfiguration.qml:34
msgid ""
"You can drag Panels and Desktops around to move them to different screens."
msgstr ""
"პანელები და სამუშაო მაგიდები სხვადასხვა ეკრანებზე შეგიძლიათ გადაათრიოთ."

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:179
msgid "Swap with Desktop on Screen %1"
msgstr "სამუშაო მაგიდასთან (ეკრანზე %1) ადგილის გაცვლა"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:180
msgid "Move to Screen %1"
msgstr "ეკრანზე გადატანა %1"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:259
msgid "%1 (primary)"
msgstr "%1 (ძირითადი)"

#: contents/explorer/AppletAlternatives.qml:63
msgid "Alternative Widgets"
msgstr "ალტერნატიული ვიჯეტები"

#: contents/explorer/AppletAlternatives.qml:163
msgid "Switch"
msgstr "გადართვა"

#: contents/explorer/AppletDelegate.qml:175
msgid "Undo uninstall"
msgstr "წაშლის გაუქმება"

#: contents/explorer/AppletDelegate.qml:176
msgid "Uninstall widget"
msgstr "ვიჯეტის წაშლა"

#: contents/explorer/Tooltip.qml:102
msgid "Author:"
msgstr "ავტორი:"

#: contents/explorer/Tooltip.qml:110
msgid "Email:"
msgstr "ელფოტა:"

#: contents/explorer/Tooltip.qml:129
msgid "Uninstall"
msgstr "ამოშლა"

#: contents/explorer/WidgetExplorer.qml:134
#: contents/explorer/WidgetExplorer.qml:245
msgid "All Widgets"
msgstr "ყველა ვიჯეტი"

#: contents/explorer/WidgetExplorer.qml:199
msgid "Widgets"
msgstr "ვიჯეტები"

#: contents/explorer/WidgetExplorer.qml:207
msgid "Get New Widgets…"
msgstr "ახალი ვიჯეტების მიღება…"

#: contents/explorer/WidgetExplorer.qml:256
msgid "Categories"
msgstr "კატეგორიები"

#: contents/explorer/WidgetExplorer.qml:339
msgid "No widgets matched the search terms"
msgstr "ვიჯეტების თქვენს ძებნას პასუხები არ აქვს"

#: contents/explorer/WidgetExplorer.qml:339
msgid "No widgets available"
msgstr "ვიჯეტების გარეშე"

#~ msgid "%1 (disabled)"
#~ msgstr "%1 (გამორთული)"
