# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Giovanni Sora <g.sora@tiscali.it>, 2019, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-11 00:48+0000\n"
"PO-Revision-Date: 2021-10-22 23:44+0200\n"
"Last-Translator: giovanni <g.sora@tiscali.it>\n"
"Language-Team: Interlingua <kde-i18n-doc@kde.org>\n"
"Language: ia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.2\n"

#: app/emojier.cpp:84 app/emojier.cpp:86
#, kde-format
msgid "Emoji Selector"
msgstr "Selector de Emoji "

#: app/emojier.cpp:88
#, kde-format
msgid "(C) 2019 Aleix Pol i Gonzalez"
msgstr "(C) 2019 Aleix Pol i Gonzalez"

#: app/emojier.cpp:90
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Giovanni Sora"

#: app/emojier.cpp:90
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "g.sora@tiscali.it"

#: app/emojier.cpp:105
#, kde-format
msgid "Replace an existing instance"
msgstr "Reimplacia un instantia existente"

#: app/ui/CategoryPage.qml:33 app/ui/emojier.qml:57
#, kde-format
msgid "Search"
msgstr "Cerca"

#: app/ui/CategoryPage.qml:74
#, kde-format
msgid "Clear History"
msgstr "Netta chronologia"

#: app/ui/CategoryPage.qml:153
#, kde-format
msgid "No recent Emojis"
msgstr "Nulle Emojis recente"

#: app/ui/emojier.qml:36
#, kde-format
msgid "%1 copied to the clipboard"
msgstr "%1 copiate a area de transferentia"

#: app/ui/emojier.qml:47
#, kde-format
msgid "Recent"
msgstr "Recente"

#: app/ui/emojier.qml:68
#, kde-format
msgid "All"
msgstr "Omne"

#: app/ui/emojier.qml:76
#, kde-format
msgid "Categories"
msgstr "Categorias"

#: emojicategory.cpp:14
msgctxt "Emoji Category"
msgid "Smileys and Emotion"
msgstr ""

#: emojicategory.cpp:15
msgctxt "Emoji Category"
msgid "People and Body"
msgstr ""

#: emojicategory.cpp:16
msgctxt "Emoji Category"
msgid "Component"
msgstr ""

#: emojicategory.cpp:17
msgctxt "Emoji Category"
msgid "Animals and Nature"
msgstr ""

#: emojicategory.cpp:18
msgctxt "Emoji Category"
msgid "Food and Drink"
msgstr ""

#: emojicategory.cpp:19
msgctxt "Emoji Category"
msgid "Travel and Places"
msgstr ""

#: emojicategory.cpp:20
msgctxt "Emoji Category"
msgid "Activities"
msgstr ""

#: emojicategory.cpp:21
msgctxt "Emoji Category"
msgid "Objects"
msgstr ""

#: emojicategory.cpp:22
msgctxt "Emoji Category"
msgid "Symbols"
msgstr ""

#: emojicategory.cpp:23
msgctxt "Emoji Category"
msgid "Flags"
msgstr ""

#~ msgid "Search…"
#~ msgstr "Cerca…"

#~ msgid "Search..."
#~ msgstr "Il cerca ..."
