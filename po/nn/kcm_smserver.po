# Translation of kcm_smserver to Norwegian Nynorsk
#
# Gaute Hvoslef Kvalnes <gaute@verdsveven.com>, 2001, 2002, 2004, 2005.
# Karl Ove Hufthammer <karl@huftis.org>, 2007, 2008, 2018, 2019, 2020, 2021, 2022.
# Eirik U. Birkeland <eirbir@gmail.com>, 2008, 2009.
msgid ""
msgstr ""
"Project-Id-Version: kcmsmserver\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-14 00:48+0000\n"
"PO-Revision-Date: 2022-10-30 09:43+0100\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.08.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: kcmsmserver.cpp:49
#, kde-format
msgid ""
"<h1>Session Manager</h1> You can configure the session manager here. This "
"includes options such as whether or not the session exit (logout) should be "
"confirmed, whether the session should be restored again when logging in and "
"whether the computer should be automatically shut down after session exit by "
"default."
msgstr ""
"<h1>Økthandsamar</h1>Du kan setja opp økthandsamaren her. Du kan velja om du "
"skal stadfesta avslutting (utlogging), om den førre økta skal gjenopprettast "
"neste gong du loggar inn og om datamaskina automatisk skal slåast av etter "
"utlogging."

#: package/contents/ui/main.qml:25
#, kde-format
msgid "Failed to request restart to firmware setup: %1"
msgstr "Feil ved førespurnad om omstart for fastvareoppsett: %1"

#: package/contents/ui/main.qml:26
#, kde-format
msgid ""
"Next time the computer is restarted, it will enter the UEFI setup screen."
msgstr ""
"Når maskina vert starta på nytt, vil ho starta innstillingane for UEFI-"
"oppsett."

#: package/contents/ui/main.qml:27
#, kde-format
msgid ""
"Next time the computer is restarted, it will enter the firmware setup screen."
msgstr ""
"Når maskina vert starta på nytt, vil ho starta innstillingane for "
"fastvareoppsett."

#: package/contents/ui/main.qml:32
#, kde-format
msgid "Restart Now"
msgstr "Start på nytt"

#: package/contents/ui/main.qml:38
#, kde-format
msgid "General:"
msgstr "Generelt:"

#: package/contents/ui/main.qml:39
#, kde-format
msgctxt "@option:check"
msgid "Confirm logout"
msgstr "Stadfest utlogging"

#: package/contents/ui/main.qml:56
#, kde-format
msgid "Default leave option:"
msgstr "Standardhandling ved avslutting:"

#: package/contents/ui/main.qml:57
#, kde-format
msgctxt ""
"@option:radio Here 'session' refers to the technical concept of session "
"restoration, whereby the windows that were open on logout are re-opened on "
"the next login"
msgid "End current session"
msgstr "Avslutt økta"

#: package/contents/ui/main.qml:67
#, kde-format
msgctxt "@option:radio"
msgid "Restart computer"
msgstr "Start datamaskina på nytt"

#: package/contents/ui/main.qml:77
#, kde-format
msgctxt "@option:radio"
msgid "Turn off computer"
msgstr "Slå av datamaskina"

#: package/contents/ui/main.qml:93
#, kde-format
msgid "When logging in:"
msgstr "Ved innlogging:"

#: package/contents/ui/main.qml:94
#, kde-format
msgctxt ""
"@option:radio Here 'session' refers to the technical concept of session "
"restoration, whereby the windows that were open on logout are re-opened on "
"the next login"
msgid "Restore last session"
msgstr "Gjenopprett førre økt"

#: package/contents/ui/main.qml:104
#, kde-format
msgctxt ""
"@option:radio Here 'session' refers to the technical concept of session "
"restoration, whereby the windows that were open on logout are re-opened on "
"the next login"
msgid "Restore last manually saved session"
msgstr "Gjenopprett førre manuelt lagra økt"

#: package/contents/ui/main.qml:114
#, kde-format
msgctxt ""
"@option:radio Here 'session' refers to the technical concept of session "
"restoration, whereby the windows that were open on logout are re-opened on "
"the next login"
msgid "Start with an empty session"
msgstr "Start med tom økt"

#: package/contents/ui/main.qml:126
#, kde-format
msgid "Don't restore these applications:"
msgstr "Ikkje gjenopprett desse programma:"

#: package/contents/ui/main.qml:143
#, kde-format
msgid ""
"Here you can enter a colon or comma separated list of applications that "
"should not be saved in sessions, and therefore will not be started when "
"restoring a session. For example 'xterm:konsole' or 'xterm,konsole'."
msgstr ""
"Her kan du skriva inn ei liste over program som ikkje skal lagrast med "
"øktene, slik at dei ikkje vert starta når ei økt vert gjenoppretta. Du kan "
"oppgje fleire program med kolon eller komma mellom, som for eksempel «xterm:"
"xconsole» eller «xterm,konsole»."

#: package/contents/ui/main.qml:152
#, kde-format
msgctxt "@option:check"
msgid "Enter UEFI setup screen on next restart"
msgstr "Gå inn i UEFI-oppsettmenyen ved neste omstart"

#: package/contents/ui/main.qml:153
#, kde-format
msgctxt "@option:check"
msgid "Enter firmware setup screen on next restart"
msgstr "Gå til fastvareoppsett-skjermen ved neste omstart"

#. i18n: ectx: label, entry (confirmLogout), group (General)
#: smserversettings.kcfg:9
#, kde-format
msgid "Confirm logout"
msgstr "Stadfest utlogging"

#. i18n: ectx: label, entry (shutdownType), group (General)
#: smserversettings.kcfg:13
#, kde-format
msgid "Default leave option"
msgstr "Standardhandling ved avslutting"

#. i18n: ectx: label, entry (loginMode), group (General)
#: smserversettings.kcfg:22
#, kde-format
msgid "On login"
msgstr "Ved innlogging"

#. i18n: ectx: label, entry (excludeApps), group (General)
#: smserversettings.kcfg:26
#, kde-format
msgid "Applications to be excluded from session"
msgstr "Program som ikkje skal reknast med i økta"
