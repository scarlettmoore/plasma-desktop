# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Bruno Patri <bruno.patri@gmail.com>, 2013.
# Thomas Vergnaud <thomas.vergnaud@gmx.fr>, 2014, 2015, 2016.
# Sebastien Renard <renard@kde.org>, 2014.
# Vincent Pinon <vpinon@kde.org>, 2017.
# Simon Depiets <sdepiets@gmail.com>, 2019.
# Xavier Besnard <xavier.besnard@neuf.fr>, 2021, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-18 00:46+0000\n"
"PO-Revision-Date: 2022-07-31 20:17+0200\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 22.04.3\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr "Général"

#: package/contents/ui/configGeneral.qml:40
#, kde-format
msgid "General:"
msgstr "Général :"

#: package/contents/ui/configGeneral.qml:42
#, kde-format
msgid "Show application icons on window outlines"
msgstr "Afficher les icônes des applications dans les contours des fenêtres"

#: package/contents/ui/configGeneral.qml:47
#, kde-format
msgid "Show only current screen"
msgstr "Afficher uniquement l'écran actuel"

#: package/contents/ui/configGeneral.qml:52
#, kde-format
msgid "Navigation wraps around"
msgstr "Navigation circulaire entre les bureaux"

#: package/contents/ui/configGeneral.qml:64
#, kde-format
msgid "Layout:"
msgstr "Disposition :"

#: package/contents/ui/configGeneral.qml:66
#, kde-format
msgctxt "The pager layout"
msgid "Default"
msgstr "Défaut"

#: package/contents/ui/configGeneral.qml:66
#, kde-format
msgid "Horizontal"
msgstr "Horizontal"

#: package/contents/ui/configGeneral.qml:66
#, kde-format
msgid "Vertical"
msgstr "Vertical"

#: package/contents/ui/configGeneral.qml:80
#, kde-format
msgid "Text display:"
msgstr "Affichage du texte : "

#: package/contents/ui/configGeneral.qml:83
#, kde-format
msgid "No text"
msgstr "Aucun texte"

#: package/contents/ui/configGeneral.qml:91
#, kde-format
msgid "Activity number"
msgstr "Numéro de l'activité"

#: package/contents/ui/configGeneral.qml:91
#, kde-format
msgid "Desktop number"
msgstr "Numéro du bureau"

#: package/contents/ui/configGeneral.qml:99
#, kde-format
msgid "Activity name"
msgstr "Nom de l'activité"

#: package/contents/ui/configGeneral.qml:99
#, kde-format
msgid "Desktop name"
msgstr "Nom du bureau"

#: package/contents/ui/configGeneral.qml:113
#, kde-format
msgid "Selecting current Activity:"
msgstr "Sélection de l'activité actuelle :"

#: package/contents/ui/configGeneral.qml:113
#, kde-format
msgid "Selecting current virtual desktop:"
msgstr "Sélection du bureau virtuel actuel :"

#: package/contents/ui/configGeneral.qml:116
#, kde-format
msgid "Does nothing"
msgstr "Ne fait rien"

#: package/contents/ui/configGeneral.qml:124
#, kde-format
msgid "Shows the desktop"
msgstr "Affiche le bureau"

#: package/contents/ui/main.qml:104
#, kde-format
msgid "…and %1 other window"
msgid_plural "…and %1 other windows"
msgstr[0] "et %1 autre fenêtre"
msgstr[1] "et %1 autres fenêtres"

#: package/contents/ui/main.qml:362
#, kde-format
msgid "%1 Window:"
msgid_plural "%1 Windows:"
msgstr[0] "%1 fenêtre:"
msgstr[1] "%1 fenêtres:"

#: package/contents/ui/main.qml:374
#, kde-format
msgid "%1 Minimized Window:"
msgid_plural "%1 Minimized Windows:"
msgstr[0] "%1 fenêtre minimisée:"
msgstr[1] "%1 fenêtres minimisées:"

#: package/contents/ui/main.qml:449
#, kde-format
msgid "Desktop %1"
msgstr "Bureau %1"

#: package/contents/ui/main.qml:450
#, kde-format
msgctxt "@info:tooltip %1 is the name of a virtual desktop or an activity"
msgid "Switch to %1"
msgstr "Basculer vers %1"

#: package/contents/ui/main.qml:595
#, kde-format
msgid "Show Activity Manager…"
msgstr "Afficher le gestionnaire d'activités…"

#: package/contents/ui/main.qml:598
#, kde-format
msgid "Add Virtual Desktop"
msgstr "Ajouter un bureau virtuel"

#: package/contents/ui/main.qml:599
#, kde-format
msgid "Remove Virtual Desktop"
msgstr "Supprimer un bureau virtuel"

#: package/contents/ui/main.qml:602
#, kde-format
msgid "Configure Virtual Desktops…"
msgstr "Configurer les bureaux virtuels..."

#~ msgid "Activate %1"
#~ msgstr "Activer %1"

#~ msgid "Icons"
#~ msgstr "Icônes"

#~ msgid "Configure Desktops..."
#~ msgstr "Configurer les bureaux…"

#~ msgid "Shows the dashboard"
#~ msgstr "Affiche le tableau de bord"

#~ msgid "Display icons:"
#~ msgstr "Afficher les icônes :"

#~ msgid "&Add Virtual Desktop"
#~ msgstr "&Ajouter un bureau virtuel"

#~ msgid "&Remove Last Virtual Desktop"
#~ msgstr "&Supprimer le dernier bureau virtuel"

#~ msgid "Configure Pager"
#~ msgstr "Configurer le gestionnaire de bureaux"

#~ msgid "Number of columns:"
#~ msgstr "Nombre de colonnes :"

#~ msgid "Number of rows:"
#~ msgstr "Nombre de lignes :"

#~ msgid "Change the number of rows"
#~ msgstr "Modifie le nombre de lignes"

#~ msgid "buttonGroupText"
#~ msgstr "buttonGroupText"

#~ msgid "buttonGroupAction"
#~ msgstr "buttonGroupAction"

#~ msgid "None"
#~ msgstr "Aucun"

#~ msgid "What will appear when the mouse is over a desktop miniature"
#~ msgstr ""
#~ "Ce qui apparaîtra lorsque la souris passe au-dessus d'un aperçu de bureau"
