# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma_shell_org\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-10 00:47+0000\n"
"PO-Revision-Date: 2022-12-12 17:31+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-IgnoreConsistency: Switch\n"
"X-POFile-SpellExtra: Ups\n"

#: contents/activitymanager/ActivityItem.qml:212
msgid "Currently being used"
msgstr "Em uso de momento"

#: contents/activitymanager/ActivityItem.qml:252
msgid ""
"Move to\n"
"this activity"
msgstr ""
"Mover para esta\n"
"actividade"

#: contents/activitymanager/ActivityItem.qml:282
msgid ""
"Show also\n"
"in this activity"
msgstr ""
"Mostrar também\n"
"nesta actividade"

#: contents/activitymanager/ActivityItem.qml:342
msgid "Configure"
msgstr "Configurar"

#: contents/activitymanager/ActivityItem.qml:359
msgid "Stop activity"
msgstr "Parar a actividade"

#: contents/activitymanager/ActivityList.qml:143
msgid "Stopped activities:"
msgstr "Actividades paradas:"

#: contents/activitymanager/ActivityManager.qml:127
msgid "Create activity…"
msgstr "Criar uma actividade…"

#: contents/activitymanager/Heading.qml:61
msgid "Activities"
msgstr "Actividades"

#: contents/activitymanager/StoppedActivityItem.qml:133
msgid "Configure activity"
msgstr "Configurar a actividade"

#: contents/activitymanager/StoppedActivityItem.qml:148
msgid "Delete"
msgstr "Apagar"

#: contents/applet/AppletError.qml:123
msgid "Sorry! There was an error loading %1."
msgstr "Infelizmente, ocorreu um erro ao carregar o %1."

#: contents/applet/AppletError.qml:161
msgid "Copy to Clipboard"
msgstr "Copiar para a Área de Transferência"

#: contents/applet/AppletError.qml:184
msgid "View Error Details…"
msgstr "Ver os Detalhes do Erro…"

#: contents/applet/CompactApplet.qml:70
msgid "Open %1"
msgstr "Abrir o %1"

#: contents/configuration/AboutPlugin.qml:19
#: contents/configuration/AppletConfiguration.qml:248
msgid "About"
msgstr "Acerca"

#: contents/configuration/AboutPlugin.qml:47
msgid "Send an email to %1"
msgstr "Enviar um e-mail para %1"

#: contents/configuration/AboutPlugin.qml:61
msgctxt "@info:tooltip %1 url"
msgid "Open website %1"
msgstr "Abrir a página Web %1"

#: contents/configuration/AboutPlugin.qml:125
msgid "Copyright"
msgstr "Copyright"

#: contents/configuration/AboutPlugin.qml:145 contents/explorer/Tooltip.qml:93
msgid "License:"
msgstr "Licença:"

#: contents/configuration/AboutPlugin.qml:148
msgctxt "@info:whatsthis"
msgid "View license text"
msgstr "Ver o texto da licença"

#: contents/configuration/AboutPlugin.qml:160
msgid "Authors"
msgstr "Autores"

#: contents/configuration/AboutPlugin.qml:170
msgid "Credits"
msgstr "Créditos"

#: contents/configuration/AboutPlugin.qml:181
msgid "Translators"
msgstr "Tradutores"

#: contents/configuration/AboutPlugin.qml:195
msgid "Report a Bug…"
msgstr "Comunicar um Erro…"

#: contents/configuration/AppletConfiguration.qml:56
msgid "Keyboard Shortcuts"
msgstr "Atalhos do Teclado"

#: contents/configuration/AppletConfiguration.qml:296
msgid "Apply Settings"
msgstr "Aplicar a Configuração"

#: contents/configuration/AppletConfiguration.qml:297
msgid ""
"The settings of the current module have changed. Do you want to apply the "
"changes or discard them?"
msgstr ""
"A configuração do módulo actual foi alterada. Deseja aplicar as alterações "
"ou esquecê-las?"

#: contents/configuration/AppletConfiguration.qml:327
msgid "OK"
msgstr "OK"

#: contents/configuration/AppletConfiguration.qml:335
msgid "Apply"
msgstr "Aplicar"

#: contents/configuration/AppletConfiguration.qml:341
#: contents/explorer/AppletAlternatives.qml:179
msgid "Cancel"
msgstr "Cancelar"

#: contents/configuration/ConfigCategoryDelegate.qml:28
msgid "Open configuration page"
msgstr "Abrir a página de configuração"

#: contents/configuration/ConfigurationContainmentActions.qml:21
msgid "Left-Button"
msgstr "Botão Esquerdo"

#: contents/configuration/ConfigurationContainmentActions.qml:22
msgid "Right-Button"
msgstr "Botão Direito"

#: contents/configuration/ConfigurationContainmentActions.qml:23
msgid "Middle-Button"
msgstr "Botão do Meio"

#: contents/configuration/ConfigurationContainmentActions.qml:24
msgid "Back-Button"
msgstr "Botão de Recuo"

#: contents/configuration/ConfigurationContainmentActions.qml:25
msgid "Forward-Button"
msgstr "Botão de Avanço"

#: contents/configuration/ConfigurationContainmentActions.qml:27
msgid "Vertical-Scroll"
msgstr "Deslocamento Vertical"

#: contents/configuration/ConfigurationContainmentActions.qml:28
msgid "Horizontal-Scroll"
msgstr "Deslocamento Horizontal"

#: contents/configuration/ConfigurationContainmentActions.qml:30
msgid "Shift"
msgstr "Shift"

#: contents/configuration/ConfigurationContainmentActions.qml:31
msgid "Ctrl"
msgstr "Ctrl"

#: contents/configuration/ConfigurationContainmentActions.qml:32
msgid "Alt"
msgstr "Alt"

#: contents/configuration/ConfigurationContainmentActions.qml:33
msgid "Meta"
msgstr "Meta"

#: contents/configuration/ConfigurationContainmentActions.qml:95
msgctxt "Concatenation sign for shortcuts, e.g. Ctrl+Shift"
msgid "+"
msgstr "+"

#: contents/configuration/ConfigurationContainmentActions.qml:167
msgctxt "@title"
msgid "About"
msgstr "Acerca"

#: contents/configuration/ConfigurationContainmentActions.qml:182
#: contents/configuration/MouseEventInputButton.qml:13
msgid "Add Action"
msgstr "Adicionar uma Acção"

#: contents/configuration/ConfigurationContainmentAppearance.qml:78
msgid "Layout changes have been restricted by the system administrator"
msgstr ""
"As mudanças de disposição estão restritas pelo administrador de sistemas"

#: contents/configuration/ConfigurationContainmentAppearance.qml:93
msgid "Layout:"
msgstr "Disposição:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:107
msgid "Wallpaper type:"
msgstr "Tipo de papel de parede:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:127
msgid "Get New Plugins…"
msgstr "Obter Novos 'Plugins'…"

#: contents/configuration/ConfigurationContainmentAppearance.qml:195
msgid "Layout changes must be applied before other changes can be made"
msgstr ""
"As mudanças de disposição devem ser aplicadas antes de se poderem fazer "
"outras alterações"

#: contents/configuration/ConfigurationContainmentAppearance.qml:199
msgid "Apply Now"
msgstr "Aplicar Agora"

#: contents/configuration/ConfigurationShortcuts.qml:16
msgid "Shortcuts"
msgstr "Atalhos"

#: contents/configuration/ConfigurationShortcuts.qml:28
msgid "This shortcut will activate the applet as though it had been clicked."
msgstr "Este atalho irá activar o elemento como se tivesse carregado nele."

#: contents/configuration/ContainmentConfiguration.qml:29
msgid "Wallpaper"
msgstr "Papel de Parede"

#: contents/configuration/ContainmentConfiguration.qml:34
msgid "Mouse Actions"
msgstr "Acções do Rato"

#: contents/configuration/MouseEventInputButton.qml:20
msgid "Input Here"
msgstr "Escreva Aqui"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:39
#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:192
#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:192
msgid "Remove Panel"
msgstr "Remover o Painel"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:47
msgid "Panel Alignment"
msgstr "Alinhamento do Painel"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:63
msgid "Top"
msgstr "Topo"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:63
msgid "Left"
msgstr "Esquerda"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:73
msgid "Center"
msgstr "Centro"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:83
msgid "Bottom"
msgstr "Fundo"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:83
msgid "Right"
msgstr "Direita"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:93
msgid "Visibility"
msgstr "Visibilidade"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:107
msgid "Always Visible"
msgstr "Sempre Visível"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:114
msgid "Auto Hide"
msgstr "Auto-Esconder"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:121
msgid "Windows Can Cover"
msgstr "As Janelas Podem Cobrir"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:128
msgid "Windows Go Below"
msgstr "As Janelas Vão Para Baixo"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:137
msgid "Opacity"
msgstr "Opacidade"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:154
msgid "Adaptive"
msgstr "Adaptativo"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:161
msgid "Opaque"
msgstr "Opaco"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:168
msgid "Translucent"
msgstr "Translúcido"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:176
msgid "Maximize Panel"
msgstr "Maximizar o Painel"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:182
msgid "Floating Panel"
msgstr "Painel Flutuante"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:198
msgid "Shortcut"
msgstr "Atalho"

#: contents/configuration/panelconfiguration/Ruler.qml:20
msgid "Drag to change maximum height."
msgstr "Arraste para mudar a altura máxima."

#: contents/configuration/panelconfiguration/Ruler.qml:20
msgid "Drag to change maximum width."
msgstr "Arraste para mudar a largura máxima."

#: contents/configuration/panelconfiguration/Ruler.qml:20
#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Double click to reset."
msgstr "Faça duplo-click para reiniciar."

#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Drag to change minimum height."
msgstr "Arraste para mudar a altura mínima."

#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Drag to change minimum width."
msgstr "Arraste para mudar a largura mínima."

#: contents/configuration/panelconfiguration/Ruler.qml:69
msgid ""
"Drag to change position on this screen edge.\n"
"Double click to reset."
msgstr ""
"Arraste para mudar a posição neste extremo do ecrã.\n"
"Faça duplo-click para reiniciar."

#: contents/configuration/panelconfiguration/ToolBar.qml:24
msgid "Add Widgets…"
msgstr "Adicionar Elementos…"

#: contents/configuration/panelconfiguration/ToolBar.qml:25
msgid "Add Spacer"
msgstr "Adicionar um Espaço"

#: contents/configuration/panelconfiguration/ToolBar.qml:26
msgid "More Options…"
msgstr "Mais Opções…"

#: contents/configuration/panelconfiguration/ToolBar.qml:222
msgctxt "Minimize the length of this string as much as possible"
msgid "Drag to move"
msgstr "Arraste para mover"

#: contents/configuration/panelconfiguration/ToolBar.qml:261
msgctxt "@info:tooltip"
msgid "Use arrow keys to move the panel"
msgstr "Usar as teclas de cursor para mover o painel"

#: contents/configuration/panelconfiguration/ToolBar.qml:282
msgid "Panel width:"
msgstr "Largura do painel:"

#: contents/configuration/panelconfiguration/ToolBar.qml:282
msgid "Panel height:"
msgstr "Altura do painel:"

#: contents/configuration/panelconfiguration/ToolBar.qml:402
#: contents/configuration/ShellContainmentConfiguration.qml:43
msgid "Close"
msgstr "Fechar"

#: contents/configuration/ShellContainmentConfiguration.qml:19
msgid "Panels and Desktops Management"
msgstr "Gestão dos Painéis e Ecrãs"

#: contents/configuration/ShellContainmentConfiguration.qml:34
msgid ""
"You can drag Panels and Desktops around to move them to different screens."
msgstr ""
"Poderá arrastar os painéis e os ecrã à vontade para os mover para ecrãs "
"diferentes."

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:179
msgid "Swap with Desktop on Screen %1"
msgstr "Trocar com o Ecrã no Ecrã Físico %1"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:180
msgid "Move to Screen %1"
msgstr "Mover para o Ecrã %1"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:259
msgid "%1 (primary)"
msgstr "%1 (primário)"

#: contents/explorer/AppletAlternatives.qml:63
msgid "Alternative Widgets"
msgstr "Elementos Alternativos"

#: contents/explorer/AppletAlternatives.qml:163
msgid "Switch"
msgstr "Mudança"

#: contents/explorer/AppletDelegate.qml:175
msgid "Undo uninstall"
msgstr "Desfazer a desinstalação"

#: contents/explorer/AppletDelegate.qml:176
msgid "Uninstall widget"
msgstr "Desinstalar o elemento"

#: contents/explorer/Tooltip.qml:102
msgid "Author:"
msgstr "Autor:"

#: contents/explorer/Tooltip.qml:110
msgid "Email:"
msgstr "E-mail:"

#: contents/explorer/Tooltip.qml:129
msgid "Uninstall"
msgstr "Desinstalar"

#: contents/explorer/WidgetExplorer.qml:134
#: contents/explorer/WidgetExplorer.qml:245
msgid "All Widgets"
msgstr "Todos os Elementos"

#: contents/explorer/WidgetExplorer.qml:199
msgid "Widgets"
msgstr "Elementos"

#: contents/explorer/WidgetExplorer.qml:207
msgid "Get New Widgets…"
msgstr "Obter Elementos Novos…"

#: contents/explorer/WidgetExplorer.qml:256
msgid "Categories"
msgstr "Categorias"

#: contents/explorer/WidgetExplorer.qml:339
msgid "No widgets matched the search terms"
msgstr "Não existem elementos correspondentes aos termos da pesquisa"

#: contents/explorer/WidgetExplorer.qml:339
msgid "No widgets available"
msgstr "Sem elementos disponíveis"
