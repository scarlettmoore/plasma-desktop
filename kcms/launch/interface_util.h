/* This file is part of the KDE libraries
    SPDX-FileCopyrightText: 2008 Laurent Montel <montel@kde.org>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#pragma once

// needed by the DBUS interface
Q_DECLARE_METATYPE(QList<int>)
